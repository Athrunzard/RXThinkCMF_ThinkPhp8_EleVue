<?php

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

return [
    // 默认缓存驱动
    'default' => 'file',

    // 缓存连接方式配置
    'stores'  => [
        'file' => [
            // 驱动方式
            'type'       => 'File',
            // 缓存保存目录
            'path'       => '',
            // 缓存前缀
            'prefix'     => '',
            // 缓存有效期 0表示永久缓存
            'expire'     => 0,
            // 缓存标签前缀
            'tag_prefix' => 'tag:',
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize'  => [],
        ],
        // Redis缓存配置
        'redis' => [
            // 驱动方式
            'type'       => 'redis',
            // 缓存地址
            'host'       => env('CACHE_HOST', '127.0.0.1'),
            // 缓存端口
            'port'       => env('CACHE_PORT', 6379),
            // 缓存密码
            'password'   => env('CACHE_PASSWORD', ''),
            // 缓存库索引
            'select'     => intval(env('CACHE_SELECT', 0)),
            // 缓存超时时间
            'timeout'    => env('CACHE_TIMEOUT', 0),
            // 全局缓存有效期（0为永久有效）
            'expire'     => env('CACHE_EXPIRE', 0),
            // 缓存持久化
            'persistent' => env('CACHE_PERSISTENT', ''),
            // 缓存前缀
            'prefix'     => env('CACHE_PREFIX', 'PRO_'),
            // 缓存标签前缀
            'tag_prefix' => env('CACHE_TAG_PREFIX', 'tag:'),
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize'  => [],
        ],
        // 更多的缓存连接
        // 更多的缓存连接
    ],
];
