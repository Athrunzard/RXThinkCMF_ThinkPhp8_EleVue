<?php
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 接口常量定义
 * @author 牧羊人
 * @since 2023/08/05
 */
const MESSAGE_OK = '操作成功';
const MESSAGE_FAILED = '操作失败';
const MESSAGE_SYSTEM_ERROR = '系统繁忙，请稍后再试';
const MESSAGE_PARAMETER_MISSING = '参数丢失';
const MESSAGE_PARAMETER_ERROR = '参数错误';
const MESSAGE_PERMISSON_DENIED = '没有权限';
const MESSAGE_INTERNAL_ERROR = '服务器繁忙，请稍后再试';
const MESSAGE_NO_TOKEN = 'Token令牌丢失';
const MESSAGE_TOKEN_FAILED = 'Token令牌验证失败';

const MESSAGE_NEEDLOGIN = '请登录';
const MESSAGE_USER_NO_INFO = '您的信息不存在';
const MESSAGE_USER_FIRBIDDEN = '您已被禁用，请联系管理员';

const MESSAGE_NO_DEVICEID = '设备号丢失';
const MESSAGE_NO_DEVICE = '未指定设备';
const MESSAGE_NO_APPVERSION = '未指定APP版本号';

const MESSAGE_NO_MOBILE = '请输入手机号';
const MESSAGE_MOBILE_INVALID = '请输入有效的手机号';
const MESSAGE_NO_PASSWORD = '请输入密码';
const MESSAGE_NO_USER_INFO = '用户信息不存在';
const MESSAGE_PASSWORD_ERROR = '手机号或密码错误';
const MESSAGE_ACCOUNT_FORBIDDEN = '手机号或密码错误';
const MESSAGE_MOBILE_REGISTERED = '用户已注册';
const MESSAGE_NO_VCODE = '请输入验证码';
